using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Tree : MonoBehaviour
{
    [SerializeField] private GameObject _stem;
    [SerializeField] private GameObject _bush;
    [SerializeField] private Transform _stemsParent;


    private List<GameObject> _stems = new List<GameObject>();

    private const float _TimeToDeleteBlock = 0.45f;


    public void CreateTree(int size)
    {
        for (int i = 0; i < size; i++)
        {
            GameObject stem = Instantiate(_stem);
            stem.transform.parent = _stemsParent;
            stem.transform.localPosition = new Vector3(0, i * 0.3f, 0);

            _stems.Add(stem);

            if (i == size - 1)
            {
                _bush.transform.localPosition = _stems[size - 1].transform.localPosition;
            }
        }
    }

    
    
    private void OnTriggerEnter(Collider collider)
    {

        GameObject player = collider.gameObject;

        bool isMovedPlayer = collider.gameObject.tag == "Player" && player.GetComponent<PlayerMovement>().IsControlled == true;


        if (isMovedPlayer )
        {
            player.GetComponentInChildren<AnimationPlayerController>().SetBool("isHit", true);
            Movement.IsManageToMove = false;

            StartCoroutine(DeleteLastBlock(collider.gameObject));
        }
    }
   
    private IEnumerator  DeleteLastBlock(GameObject player)
    {
        yield return new WaitForSeconds(_TimeToDeleteBlock);
       
        if (_stems.Count == 1)
        {
            AudioManager.TreeDestroy();
            Destroy(gameObject);
        }

        Destroy(_stems[0]);
        _stems.RemoveAt(0);

        _stemsParent.Translate(new Vector3(0, -0.3f, 0));

        PlayerManager.GiveToPool(player);
        PlayerMovement.XPointToMove = transform.localPosition.x;

        DontMoveIfLose();


        AudioManager.TreeHit();

    }

    private void DontMoveIfLose()
    {
        if (PlayerPool.CurrentCount <= 0)
        {
            Movement.IsManageToMove = false;
        }
        else
        {
            Movement.IsManageToMove = true;
        }
    }
}
