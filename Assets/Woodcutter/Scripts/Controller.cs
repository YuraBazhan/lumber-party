using UnityEngine;

public class Controller : MonoBehaviour
{
    private const float _sensivityCoeff = 0.005f;
    

    private void FixedUpdate()
    {
        CheckTouches();
    }

    private void CheckTouches()
    {
        if (Input.touches.Length > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                float newX = PlayerMovement.XPointToMove + (touch.deltaPosition.x * _sensivityCoeff);

                SetNewXPointToMove(newX);
            }
        }
    }
    private void SetNewXPointToMove(float xPosition)
    {
        PlayerMovement.XPointToMove = xPosition;
    }
}
