using UnityEngine;

public class Movement : MonoBehaviour
{
    private static bool _isManageToMove = false;

    private float _forwardSpeed = 2.0f;


    public static bool IsManageToMove
    {
        set { _isManageToMove = value; }
    }

    private void FixedUpdate()
    {
        if (_isManageToMove == true)
        {
            MoveForward();
        }
    }

    private void MoveForward()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * _forwardSpeed);
    }
}
