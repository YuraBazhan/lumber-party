using System.Collections.Generic;
using UnityEngine;

public class PlayerPool : MonoBehaviour
{
    public static PlayerPool Instance;

    [SerializeField] private GameObject _playerPrefab;

    private static int _currentCountOfPlayers = 1;

    private List<GameObject> _playersPooled = new List<GameObject>();
    private int _maxCountOfPlayers = 30;

    private int _currentControledPlayer = 0;


    public static int CurrentCount
    {
        get { return _currentCountOfPlayers; }
        set { _currentCountOfPlayers = value; }
    }

    public GameObject GetPooledObject(Vector3 position)
    {
        for (int i = _currentControledPlayer; i < _playersPooled.Count; i++)
        {
            if (!_playersPooled[i].activeInHierarchy == true)
            {
                _playersPooled[i].transform.position = position;
                _playersPooled[i].SetActive(true);
                CurrentCount++;

                UpdateCountOfPersonsUI();
                AudioManager.UpdateSpeedOfMelody();

                return _playersPooled[i];
            }
        }
        return null;
    }
    public void SetPooledObject(GameObject player)
    {
        player.GetComponent<PlayerMovement>().IsControlled = false;
        player.SetActive(false);
        SetPlayerAsNotChild(player);
        CurrentCount--;

        UpdateCountOfPersonsUI();
        MakeCurrent(++_currentControledPlayer);

        AudioManager.UpdateSpeedOfMelody();
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    private void Start()
    {
        SpawnPlayers();
    }
    private void SpawnPlayers()
    {
        for (int i = 0; i < _maxCountOfPlayers; i++)
        {
            GameObject player = Instantiate(_playerPrefab, Vector3.zero, Quaternion.identity);
            PlayerMovement playerMovement = player.GetComponent<PlayerMovement>();


            if (i == 0)
            {
                SetPlayerAsChild(player);
            }
            else
            {
                playerMovement.FollowedPlayer = _playersPooled[i - 1];
                playerMovement.IsControlled = false;
                player.SetActive(false);
            }

            _playersPooled.Add(player);
        }

        _playersPooled[0].GetComponent<PlayerMovement>().FollowedPlayer = _playersPooled[_maxCountOfPlayers - 1];
        MakeCurrent(_currentControledPlayer);
    }
    private void MakeCurrent(int index)
    {
        CameraFollower.Target = _playersPooled[index].transform;

        SetPlayerAsChild(_playersPooled[index]);
        _playersPooled[index].GetComponent<PlayerMovement>().IsControlled = true;
    }

    private void SetPlayerAsChild(GameObject player)
    {
        player.transform.parent = gameObject.transform.parent;
        player.transform.localPosition = new Vector3(player.transform.localPosition.x, 0, 0);
    }
    private void SetPlayerAsNotChild(GameObject player)
    {
        player.transform.parent = null;
    }
    private void Lose()
    {
        MainUI.ShowRestartButton();
        PlayerMovement.IsManageToMoveOnSides = false;
    }
    private void UpdateCountOfPersonsUI()
    {
        if (CurrentCount >= _maxCountOfPlayers)
        {
            CountPlayersUI.UpdateCountText("Max");
        }
        else if (CurrentCount <= 0)
        {
            Lose();
            CountPlayersUI.UpdateCountText("");
        }
        else
        {
            CountPlayersUI.UpdateCountText(CurrentCount.ToString());
        }
    }
}
