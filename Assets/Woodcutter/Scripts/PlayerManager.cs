using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static void TakeFromPool(Vector3 position)
    {
        PlayerPool.Instance.GetPooledObject(position);
    }
    public static void GiveToPool(GameObject gameObject)
    {
        PlayerPool.Instance.SetPooledObject(gameObject);
    }


    private void Start()
    {
        TakeFromPool(Vector3.zero);
    }
}
