using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private static float _xPointToMove;

    private static bool _isManageToMoveOnSides = false;

    private static float _maxXPosition = 0.8f;
    private static float _minXPosition = -0.8f;
    private float _sideSpeed = 5;

    private GameObject _FollowedPlayer;
    private float _speedToFollow = 8;
    private float _coeffDistanceBetwenPlayers = 0.4f;

    private bool _isControlled = false;


    public static float XPointToMove
    {
        get { return _xPointToMove; }
        set { _xPointToMove = value; }
    }
    
    public static bool IsManageToMoveOnSides
    {
        set { _isManageToMoveOnSides = value; }
    }
    public bool IsControlled
    {
        get { return _isControlled; }
        set { _isControlled = value; }
    }
    public GameObject FollowedPlayer
    {
        private get { return _FollowedPlayer; }
        set { _FollowedPlayer = value; }
    }

    public static void SetRandomXPositionToMove()
    {
        _xPointToMove = Random.RandomRange(_minXPosition, _maxXPosition);
    }

    private void FixedUpdate()
    {
        if (IsControlled == true)
        {
            Move();
        }
        else
        {
            Follow();
        }
    }


    private void Move()
    {
        if (_isManageToMoveOnSides == true)
        {
            MoveOnSides();
        }
    }

    private void MoveOnSides()
    {

        ClampXPosition();

        transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(_xPointToMove, transform.localPosition.y, transform.localPosition.z), Time.deltaTime * _sideSpeed);
    }
    private void ClampXPosition()
    {
        _xPointToMove = Mathf.Clamp(_xPointToMove, _minXPosition, _maxXPosition);
    }

    private void Follow()
    {
        Vector3 _offset = -FollowedPlayer.transform.forward * _coeffDistanceBetwenPlayers;
        Vector3 desiredPosition = FollowedPlayer.transform.position + _offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, _speedToFollow * Time.deltaTime);

        transform.position = new Vector3(smoothedPosition.x, transform.position.y, smoothedPosition.z);

        transform.LookAt(FollowedPlayer.transform);

    }
}
