using UnityEngine;

public class AnimationPlayerController : MonoBehaviour
{
    private Animator _animator;




    
    public void SetBool(string name, bool value)
    {
        _animator.SetBool(name, value);
    }
    
    void Start()
    {
        _animator = GetComponent<Animator>();
    }
}
