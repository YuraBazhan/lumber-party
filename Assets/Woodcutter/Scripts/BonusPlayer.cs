using UnityEngine;

public class BonusPlayer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        bool isMainPlayer = other.tag == "Player" && other.gameObject.GetComponent<PlayerMovement>().IsControlled == true;

        if (isMainPlayer == true)
        {
            PlayerManager.TakeFromPool(transform.position);
            AudioManager.TakeWoodcutter();

            Destroy(gameObject);
        }
    }
}
