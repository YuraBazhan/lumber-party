using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    [SerializeField] private float _speed;

    private static Transform _target;

    private Vector3 _standartOffset = new Vector3(0, 2.88f, -3);
    private Vector3 _offset;

    private const float _YStep = 0.1f, _ZStep = -0.17f;


    public static Transform Target
    {
        set { _target = value; }
    }
    private void Start()
    {
        _offset = _standartOffset;
    }

    void FixedUpdate()
    {
        Follow();
    }

    private void Follow()
    {
        Vector3 desiredPosition = _target.localPosition + CalculateYDistance();
        Vector3 smoothedPosition = Vector3.Lerp(transform.localPosition, desiredPosition, _speed * Time.deltaTime);
        transform.localPosition = new Vector3(transform.localPosition.x, smoothedPosition.y, smoothedPosition.z);
    }
    private Vector3 CalculateYDistance()
    {
        if (PlayerPool.CurrentCount == 1)
        {
            return _standartOffset;
        }

        _offset = new Vector3(_offset.x, _standartOffset.y + (PlayerPool.CurrentCount * _YStep), _standartOffset.z + (PlayerPool.CurrentCount * _ZStep));

        return _offset;
    }
}
