using DG.Tweening;
using UnityEngine;


public class TurnPlatform : MonoBehaviour
{
    public enum Direction
    {
        Left = -90,
        Right = 90,
    }

    [SerializeField] private Direction _route = Direction.Left;

    [SerializeField] private Transform _pointToMove;
    
    private bool _isTurned = false;
    private float _timeToTurn = 1.0f;


    public Direction Route
    {
        get { return _route; }
        set { _route = value; }
    }



    private void OnTriggerEnter(Collider other)
    {
        GameObject collideObject = other.gameObject;

        bool isMainPlayer = collideObject.tag == "Player" && collideObject.GetComponent<PlayerMovement>().IsControlled == true;

        if (isMainPlayer == true && _isTurned == false)
        {
            TurnPlayers(other.transform.parent.gameObject);
        }
    }
    private void TurnPlayers(GameObject woodCutters)
    {
        Movement.IsManageToMove = false;
        PlayerMovement.IsManageToMoveOnSides = false;
        PlayerMovement.XPointToMove = 0;

        GameObject controlledPlayer = woodCutters.transform.Find("Player(Clone)").gameObject;


        DOTurn(woodCutters, controlledPlayer);


        _isTurned = true;
    }
    private void DOTurn(GameObject woodCutters, GameObject controlledPlayer)
    {
        Sequence secuence = DOTween.Sequence();
        secuence.Append(woodCutters.transform.DORotate(new Vector3(0, (int)_route, 0), _timeToTurn, RotateMode.WorldAxisAdd));

        secuence.Join(woodCutters.transform.DOMove(_pointToMove.position, _timeToTurn).OnComplete(() => Movement.IsManageToMove = true));
        secuence.Join(controlledPlayer.transform.DOLocalMoveX(Vector3.zero.x, _timeToTurn).OnComplete(() => PlayerMovement.IsManageToMoveOnSides = true));
    }
}
