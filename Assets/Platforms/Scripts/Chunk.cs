using UnityEngine;

public class Chunk : MonoBehaviour
{
    [SerializeField] private GameObject _tree;
    [SerializeField] private GameObject _playerBonus;


    private static bool _isFirst = true;


    private const int _minSizeOfTree = 2;
    private const int _maxSizeOfTree = 4;

    private const float _DistanceBetweenTreesInLine = 0.48f;
    private const float _startPositionOfLine = -0.7f;

    private const int _minCountOfBonusPlayer = 2;
    private const int _maxCountOfBonusPlayer = 3;

    private const float _minXPositionOfBonusPlayer = -0.8f;
    private const float _maxXPositionOfBonusPlayer = 0.8f;
    private const float _minZPositionOfBonusPlayer = -1.5f;
    private const float _maxZPositionOfBonusPlayer = 1.4f;



    public static bool IsFirst
    {
        set { _isFirst = value; }
    }

    void Start()
    {
        GenerateChunk(2.0f);
    }

    private void GenerateChunk(float zPos)
    {
        GenerateLineOfTrees(zPos);
        GenerateBonusPlayers();
    }

    private void GenerateLineOfTrees(float zPos)
    {
        for (int i = 0; i < 4; i++)
        {
            float xPosition = _startPositionOfLine + (_DistanceBetweenTreesInLine * i);

            GameObject tree = Instantiate(_tree);
            tree.transform.parent = gameObject.transform;

            if (i % 2 == 0)
            {
                tree.transform.localPosition = new Vector3(xPosition, 0.3f, zPos);
            }
            else
            {
                tree.transform.localPosition = new Vector3(xPosition, 0.3f, zPos + 0.025f);
            }

            tree.GetComponent<Tree>().CreateTree(Random.Range(_minSizeOfTree, _maxSizeOfTree + 1));
        }
    }
    private void GenerateBonusPlayers()
    {
        int count;

        if (_isFirst == true)
        {
            count = _maxSizeOfTree;
            _isFirst = false;
        }
        else
        {
            count = Random.Range(_minCountOfBonusPlayer, _maxCountOfBonusPlayer + 1);
        }

        for (int i = 0; i < count; i++)
        {
            Vector2 randomPosition = new Vector2(Random.Range(_minXPositionOfBonusPlayer, _maxXPositionOfBonusPlayer), Random.Range(_minZPositionOfBonusPlayer, _maxZPositionOfBonusPlayer));

            GameObject bonus = Instantiate(_playerBonus, new Vector3(0, 0, 0), transform.rotation);
            bonus.transform.parent = gameObject.transform;
            bonus.transform.localPosition = new Vector3(randomPosition.x, 0, randomPosition.y);
        }
    }
}
