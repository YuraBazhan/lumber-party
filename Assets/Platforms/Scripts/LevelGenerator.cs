using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private int _levelLenght;
    [SerializeField] private GameObject _chunk;
    [SerializeField] private GameObject _finish;


    private const float _DistanceBetweenChunks = 6;


    private void Start()
    {
        GenerateChunks();
    }
    private void GenerateChunks()
    {
        for (int i = 0; i < _levelLenght; i++)
        {
            Instantiate(_chunk, new Vector3(0, 0, i * _DistanceBetweenChunks), Quaternion.identity);
        }
        Instantiate(_finish, new Vector3(0, 0, _levelLenght * _DistanceBetweenChunks), Quaternion.identity);
    }


}
