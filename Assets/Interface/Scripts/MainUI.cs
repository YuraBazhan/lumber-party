using UnityEngine;
using UnityEngine.SceneManagement;


public class MainUI : MonoBehaviour
{
    [SerializeField] private GameObject _setRestartButton;
    [SerializeField] private GameObject _setContinueButton;


    private static GameObject _restartButton;
    private static GameObject _continueButton;


    public static void ShowRestartButton()
    {
        _restartButton.SetActive(true);
    }

    public static void ShowContinueButton()
    {
        _continueButton.SetActive(true);
    }

    
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        ResetGameData();
    }

    private void Start()
    {
        SetStaticFields();
    }
    private void SetStaticFields()
    {
        _restartButton = _setRestartButton;
        _continueButton = _setContinueButton;
    }
    private void ResetGameData()
    {
        Chunk.IsFirst = true;
        PlayerPool.CurrentCount = 1;
        Movement.IsManageToMove = false;
        PlayerMovement.SetRandomXPositionToMove();
    }
}
