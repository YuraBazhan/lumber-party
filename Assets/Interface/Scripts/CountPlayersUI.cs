using UnityEngine;
using UnityEngine.UI;


public class CountPlayersUI : MonoBehaviour
{
    [SerializeField] private Text _setPlayerCountText;

    private static Text _playerCountText;




    public static void UpdateCountText(string count)
    {
        _playerCountText.text = count;
    }

    private void Awake()
    {
        SetFields();
        UpdateCountText(PlayerPool.CurrentCount.ToString());
    }
    private void SetFields()
    {
        _playerCountText = _setPlayerCountText;
    }
}
