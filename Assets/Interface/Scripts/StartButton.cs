using UnityEngine;
using UnityEngine.EventSystems;

public class StartButton : MonoBehaviour, ISelectHandler 
{
    public void OnSelect(BaseEventData eventData)
    {
        StartGame();
    }

    private void StartGame()
    {
        Movement.IsManageToMove = true;
        PlayerMovement.IsManageToMoveOnSides = true;
        PlayerMovement.SetRandomXPositionToMove();

        gameObject.SetActive(false);
    }
}
