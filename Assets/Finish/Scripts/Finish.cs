using System.Collections;
using UnityEngine;

public class Finish : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(WinGame());
        }
    }
    private IEnumerator WinGame()
    {
        CameraFollower.Target = transform;

        yield return new WaitForSeconds(0.75f);

        Movement.IsManageToMove = false;
        MainUI.ShowContinueButton();
        AudioManager.Win();

        SetDanceAnimations();

        Destroy(this);
    }
    private void SetDanceAnimations()
    {
        AnimationPlayerController[] _animControllers = FindObjectsOfType<AnimationPlayerController>();

        for (int i = 0; i < _animControllers.Length; i++)
        {
            _animControllers[i].SetBool("isWin", true);
        }
    }
}
