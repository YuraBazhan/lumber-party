using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource _setTreeHit;
    [SerializeField] private AudioSource _setMelody;
    [SerializeField] private AudioSource _setTreeDestroy;
    [SerializeField] private AudioSource _setWin;
    [SerializeField] private AudioSource _setTakeWoodcutter;



    private static AudioSource _treeHit;
    private static AudioSource _melody;
    private static AudioSource _treeDestroy;
    private static AudioSource _win;
    private static AudioSource _takeWoodcutter;


    private const float _SpeedUpValue = 0.015f;

    public static void TreeHit()
    {
        _treeHit.Play();
    }
    public static void TreeDestroy()
    {
        _treeDestroy.Play();
    }
    public static void Win()
    {
        _win.Play();
    }
    public static void TakeWoodcutter()
    {
        _takeWoodcutter.Play();
    }
    public static void UpdateSpeedOfMelody()
    {
        _melody.pitch = 1 + (PlayerPool.CurrentCount * _SpeedUpValue) ;
    }

    private void Start()
    {
        SetFields();

        _melody.Play();
    }
    private void SetFields()
    {
        _treeHit = _setTreeHit;
        _melody = _setMelody;
        _treeDestroy = _setTreeDestroy;
        _win = _setWin;
        _takeWoodcutter = _setTakeWoodcutter;
    }

}
